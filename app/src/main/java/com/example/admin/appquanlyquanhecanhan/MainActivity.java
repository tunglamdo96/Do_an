package com.example.admin.appquanlyquanhecanhan;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.multidex.MultiDex;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.admin.appquanlyquanhecanhan.Adapter.AdapterViewPager;
import com.example.admin.appquanlyquanhecanhan.Database.Database;
import com.example.admin.appquanlyquanhecanhan.Model.Nhom;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,TabHost.OnTabChangeListener {

    Toolbar toolbar;
    public static MaterialSearchView searchView;
    public static TabHost tabHost;
    public static ViewPager viewPager;
    AdapterViewPager adapterViewPager;
    public  static FloatingActionButton floatingActionButton;
    public static final String Database_name="CSDL.sqlite";
    public SQLiteDatabase database;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        taoActionBar();
        TaoTabs();
        FloatingAction();
    }


    private void FloatingAction() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ThemLienHe.class);
                startActivity(intent);
            }
        });
    }

    private void TaoTabs() {
       tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("LienHe");
        tab1.setIndicator("Liên hệ");
        tab1.setContent(new NoiDungAo(MainActivity.this));

        TabHost.TabSpec tab2 = tabHost.newTabSpec("Nhom");
        tab2.setIndicator("Nhóm");
        tab2.setContent(new NoiDungAo(MainActivity.this));

        TabHost.TabSpec tab3 = tabHost.newTabSpec("UaThich");
        tab3.setIndicator("Mục ưa thích");
        tab3.setContent(new NoiDungAo(this));

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
        tabHost.setOnTabChangedListener(this);
        viewPager.setOnPageChangeListener(this);
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            View v = tabHost.getTabWidget().getChildAt(i);
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.white));
        }


    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String s) {
        int vitri = tabHost.getCurrentTab();
        viewPager.setCurrentItem(vitri);
        if (viewPager.getCurrentItem() == 1){
            MainActivity.floatingActionButton.setVisibility(View.INVISIBLE);
        }else {
            MainActivity.floatingActionButton.setVisibility(View.VISIBLE);
        }

    }

    class NoiDungAo implements TabHost.TabContentFactory {

        Context context;
        public NoiDungAo(Context context){
            this.context = context;
        }
        public View createTabContent(String s) {
            View view = new View(context);
            view.setMinimumWidth(0);
            view.setMinimumHeight(0);
            return view;
        }
    }



    private void taoActionBar() {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);

    }
    private void anhXa() {
        toolbar = findViewById(R.id.toolBar);
        searchView = findViewById(R.id.meterialSearch);
        tabHost = findViewById(R.id.tabHost);
        viewPager = findViewById(R.id.viewPager);
        adapterViewPager = new AdapterViewPager(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        floatingActionButton = findViewById(R.id.fab);
        database = Database.initDatabase(MainActivity.this,Database_name);


    }
    // tao menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuSearch:
                searchView.setMenuItem(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(MainActivity.this);
    }
}
